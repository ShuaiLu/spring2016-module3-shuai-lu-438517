<?php
//delete the comment with comment_id=cid , cid is the primary key of table comments.
$id=$_GET['id'];
$cid=$_GET['cid'];
require 'database.php';
      $stmt = $mysqli->prepare("delete from comments where comment_id=?");
      if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;}
    $stmt->bind_param('i', $cid);
    $stmt->execute();
    $stmt->close();
     header("Location:read.php?id=$id");
?>