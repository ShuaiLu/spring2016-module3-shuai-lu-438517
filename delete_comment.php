<?php

//delete the comment if the comment is make by the user, otherwise redirect to home page;
session_start();
$username=$_SESSION['username'];
$id=$_GET['id'];
$cid=$_GET['cid'];

require 'database.php';
      $stmt = $mysqli->prepare("select name from comments where comment_id=?");
      if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;}
    $stmt->bind_param('i', $cid);
    $stmt->execute();
    $stmt->bind_result($thename);
    if($stmt->fetch())
    {
        if($thename==$username)
        {
            header("Location:comment_deleteit.php?cid={$cid}&id={$id}");
            exit;
        }
    }
    $stmt->close();
    header("Location:read.php?id=$id");


?>