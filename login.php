<?php
// if the user exist, go to home page. Other wise go back.
session_start();
require 'database.php';
$username = $_POST['user'];
$password = $_POST['pass'];
$stmt = $mysqli->prepare("select username,password from users where username=?");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
$stmt->bind_param('s', $username);
$stmt->execute();
$stmt->bind_result($first, $last);
$stmt->fetch();

    if(password_verify($password,$last))
    {
     $_SESSION['username']=$first;
     header('Location:index.php');
     exit;
    }

$stmt->close();
 header('Location:login_page.php?msg=error');
?>