<?php
//post news and insert it to the table.
session_start();
$headline=$_POST['headline'];
$story=$_POST['story'];
$username=$_SESSION['username'];
require 'database.php';
 $stmt = $mysqli->prepare("insert into news(headline,story,name,timestamp) values(
                          ?,?,?,?)");
    if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;}
    $time=date('Y-m-d H:i:s');
    $stmt->bind_param('ssss',$headline,$story,$username,$time);
    $stmt->execute();
    $stmt->close();
    header('Location:index.php');
?>