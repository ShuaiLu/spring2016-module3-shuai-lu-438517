<?php
session_start();
$id=$_POST['id'];
$comment=$_POST['comment'];
if(!isset($_SESSION['username']))
{
    header("Location:read.php?id=$id");
    exit;
}
$username=$_SESSION['username'];



require 'database.php';
      $stmt = $mysqli->prepare("insert into comments(story_id,comment_content,name,timestamp)
                               values(?,?,?,?)");
      
      if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
$time=date('Y-m-d H:i:s');
    $stmt->bind_param('isss',$id,$comment,$username,$time);
    $stmt->execute();
    $stmt->close();
header("Location:read.php?id=$id");  
?>